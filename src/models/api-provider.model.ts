import { BehaviorSubject } from 'rxjs';

export interface APIProvider<T> {
  getAPI$: () => BehaviorSubject<T | null>
}