export interface AppError {
  errMsg: string;
  originErr?: any;
}

export class Err extends Error {
  constructor(msg: string, origin = null) {
    super(msg);
  }
}