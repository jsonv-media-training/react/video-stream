import { Observable } from 'rxjs';
import { User } from './user.model';

export interface AuthService {
  authType: AuthType;
  isLoggedIn: () => Observable<boolean>;
  getUser: (...args: any[]) => Observable<User>;
  login: (...args: any[]) => Observable<User>;
  logout: () => Observable<void>;
  // addStatusListener: (handler: (isSignedIn: boolean) => void) => Observable<void>
}

export type AuthServiceFactory = (name: AuthType) => AuthService

export enum AuthType {
  Google = 'google',
  FaceBook = 'facebook'
}