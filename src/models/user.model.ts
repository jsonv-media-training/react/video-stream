import { AuthType } from './auth.model';

export interface User {
  id: string;
  name?: string;
  email?: string;
  image?: string;
  authType?: AuthType;
}