import { ThunkAction } from 'redux-thunk';
import { take, tap } from 'rxjs/operators';

import { User, Action, AuthService, AuthType } from '../../models';

export enum AUTH_ACTION_TYPE {
  SIGN_IN_ACTION = 'SIGN_IN_ACTION',
  SIGN_IN_FAILED_ACTION = 'SIGN_IN_FAILED_ACTION',
  SIGN_IN_SUCCESS_ACTION = 'SIGN_IN_SUCCESS_ACTION',
  SIGN_OUT_ACTION = 'SIGN_OUT_ACTION'
}

export const signInAction = (): Action<any> => {
  return {
    type: AUTH_ACTION_TYPE.SIGN_IN_ACTION,
    payload: null
  }
}

export const signInFailedAction = (message: string): Action<string> => {
  return {
    type: AUTH_ACTION_TYPE.SIGN_IN_FAILED_ACTION,
    payload: message
  }
}

export const signInSuccessAction = (user: User, authType: AuthType): Action<User> => {
  return {
    type: AUTH_ACTION_TYPE.SIGN_IN_SUCCESS_ACTION,
    payload: { ...user, authType}
  }
}

export const signOutAction = (): Action<any> => {
  return {
    type: AUTH_ACTION_TYPE.SIGN_OUT_ACTION,
  }
}

export const signInWithServiceAction = (authService: AuthService): ThunkAction<void, any, null, Action<User | string>> => {
  console.log(authService);
  return (dispatch) => {
    authService.login()
      .pipe(
        tap(() => dispatch(signInAction())),
        take(1)
      )
      .subscribe(
        res => dispatch(signInSuccessAction(res as User, authService.authType)),
        (error: Error) => dispatch(signInFailedAction(error?.message))
      )
  }
}

export const signOutWithServiceAction = (authService: AuthService): ThunkAction<void, any, null, Action<User | string>> => {
  return (dispatch) => {
    authService.logout()
      .pipe(take(1))
      .subscribe(() => dispatch(signOutAction()))
  }
}








