import { User, Action, State } from '../../models';
import { AUTH_ACTION_TYPE } from './auth.action';

const initialState: State<User> = {
  data: null,
  loading: false,
  error: ''
};

const reducer = (state = initialState, action: Action<User | string>) => {
  switch (action.type) {
    case AUTH_ACTION_TYPE.SIGN_IN_ACTION:
      return { ...state, loading: true, error: '' };
    case AUTH_ACTION_TYPE.SIGN_IN_SUCCESS_ACTION:
      return  { ...state, loading: false, error: '', data: action.payload as User };
    case AUTH_ACTION_TYPE.SIGN_IN_FAILED_ACTION:
      return  { ...state, loading: false, error: action.payload as string, data: null };
    case AUTH_ACTION_TYPE.SIGN_OUT_ACTION:
      return  { ...state, loading: false, error: '', data: null }
    default:
      return state;
  }
};

export default reducer;