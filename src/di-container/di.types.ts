const TYPES = {
  GoogleAPIProvider: Symbol('GoogleAPIProvider'),
  FacebookAPIProvider: Symbol('FacebookAPIProvider'),
  AuthService: Symbol("AuthService"),
  ServiceFactory: Symbol('ServiceFactory'),
};


export default TYPES;