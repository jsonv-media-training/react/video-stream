import 'reflect-metadata';
import { Container, interfaces } from 'inversify';
import TYPES from './di.types';

import { GoogleAuthService } from '../services/google-auth.service';
import { FacebookAuthService } from '../services/facebook-auth.service';

import { GoogleAPIProvider } from '../services/gapi-provider.service';
import { FaceBookAPIProvider } from '../services/fbapi-provider.service';
import { APIProvider, AuthService, AuthType } from '../models';
import { FaceBook, Google } from '../app/extended-type';

const container = new Container();
container.bind<APIProvider<Google.GoogleAuth>>(TYPES.GoogleAPIProvider)
  .to(GoogleAPIProvider)
  .inSingletonScope();

container.bind<APIProvider<FaceBook.FacebookStatic>>(TYPES.FacebookAPIProvider)
  .to(FaceBookAPIProvider)
  .inSingletonScope();

container.bind<AuthService>(TYPES.AuthService)
  .to(GoogleAuthService)
  .inSingletonScope()
  .whenTargetNamed(AuthType.Google);

container.bind<AuthService>(TYPES.AuthService)
  .to(FacebookAuthService)
  .inSingletonScope()
  .whenTargetNamed(AuthType.FaceBook);

container.bind<interfaces.Factory<AuthService>>(TYPES.ServiceFactory)
  .toFactory((context: interfaces.Context) => (name: AuthType) => {
    return context.container.getNamed<AuthService>(TYPES.AuthService, name)
  });

export default container;






