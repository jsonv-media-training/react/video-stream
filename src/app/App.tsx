import React, { FunctionComponent } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import { Provider } from 'react-redux';
import store from '../redux/store.redux';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ThemeProvider } from '@material-ui/core';
import Container from '@material-ui/core/Container';

import StreamList from '../components/streams/stream-list/StreamList';
import StreamCreate from '../components/streams/stream-create/StreamCreate';
import StreamEdit from '../components/streams/stream-edit/StreamEdit';
import StreamDelete from '../components/streams/stream-delete/StreamDelete';
import StreamShow from '../components/streams/stream-show/StreamShow';
import Nav from '../components/nav/Nav';

import './App.scss';
import theme from './theme.mui';

import container from '../di-container/di.config';
import { DiContext } from '../context/di.context'

const App: FunctionComponent<any> = () => {
  return (
    <DiContext.Provider value={container}>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <Nav/>
            <Container style={{ padding: '15px 25px' }}>
              <Route path={'/'} exact component={StreamList}/>
              <Route path={'/streams/new'} exact component={StreamCreate}/>
              <Route path={'/streams/edit'} exact component={StreamEdit}/>
              <Route path={'/streams/delete'} exact component={StreamDelete}/>
              <Route path={'/streams/show'} exact component={StreamShow}/>
            </Container>
          </BrowserRouter>
        </ThemeProvider>
      </Provider>
    </DiContext.Provider>
  );
}

export default App;
