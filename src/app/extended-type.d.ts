// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ButtonPropsColorOverrides } from '@material-ui/core/Button/Button';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { AppBarPropsColorOverrides } from '@material-ui/core/AppBar/AppBar';

declare module '@material-ui/core/Button/Button' {
  interface ButtonPropsColorOverrides {
    error: true;
    info: true;
    success: true;
    warning: true;
  }
}

declare module '@material-ui/core/AppBar/AppBar' {
  interface AppBarPropsColorOverrides {
    error: true;
    info: true;
    success: true;
    warning: true;
  }
}

declare module FaceBook {
  export type FacebookStatic = fb.FacebookStatic;
  export type UserField = fb.UserField;
  export interface StatusResponse extends fb.StatusResponse {}
  export interface LoginOptions extends fb.LoginOptions {}
  export interface Error {
    error: {
      code: number;
      fbtrace_id?: string;
      message: string;
      type?: string;
    }
  }
  export interface ProfilePicture {
    data: {
      url: string;
    }
  }
}

declare module Google {
  export type GoogleAuth = gapi.auth2.GoogleAuth;
  export type GoogleUser = gapi.auth2.GoogleUser;
  export interface GoogleError {
    detail?: string;
    error: string
  }
}




