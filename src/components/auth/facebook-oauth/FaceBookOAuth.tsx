import React, { FunctionComponent, useContext } from 'react';
import { signInWithServiceAction } from '../../../redux/auth/auth.action';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';

import Facebook from '@material-ui/icons/Facebook';
import Button from '@material-ui/core/Button';

import TYPES from '../../../di-container/di.types';
import { DiContext } from '../../../context/di.context';
import { Action, AuthService, AuthServiceFactory, AuthType, User } from '../../../models';
import './FaceBookOAuth.scss';

const FaceBookOAuth: FunctionComponent<{ signInWithServiceAction: (service: AuthService) => void }> = ({ signInWithServiceAction }) => {

  const container = useContext(DiContext);
  const fbAuthService: AuthService = container.get<AuthServiceFactory>(TYPES.ServiceFactory)(AuthType.FaceBook);

  return (
    <Button
      variant="contained"
      color="primary"
      size="large"
      fullWidth
      className={'facebook-btn'}
      startIcon={<Facebook/>}
      onClick={() => signInWithServiceAction(fbAuthService)}
    >
      Login with facebook
    </Button>

  );
}

const mapDispatchToProps = (dispatch: ThunkDispatch<void, any, Action<User | string>>) => ({
  signInWithServiceAction: (authService: AuthService) => dispatch(signInWithServiceAction(authService))
});

export default connect(null, mapDispatchToProps)(FaceBookOAuth);