import React, { FunctionComponent, useContext, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { signInSuccessAction, signOutAction } from '../../redux/auth/auth.action';

import { of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';

import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';

import { AnimatePresence, motion } from 'framer-motion';
import { logInButton } from './variants.motion';

import AuthPanel from './auth-panel/AuthPanel';
import UserPanel from './user-panel/UserPanel';

import TYPES from '../../di-container/di.types';
import { DiContext } from '../../context/di.context';
import { User, AuthService, AppState, AuthServiceFactory, AuthType } from '../../models';
import { AuthProps } from './props';
import './Auth.scss';

const Auth: FunctionComponent<AuthProps> = ({ user, signOutAction, signInSuccessAction }) => {

  const [isDrawerOpen, setIsDrawerOpen] = useState<boolean>(false);
  const container = useContext(DiContext);

  useEffect(() => {
    const googleAuthService: AuthService = container.get<AuthServiceFactory>(TYPES.ServiceFactory)(AuthType.Google);
    const fbAuthService: AuthService = container.get<AuthServiceFactory>(TYPES.ServiceFactory)(AuthType.FaceBook);
    const subscription = googleAuthService.isLoggedIn()
      .pipe(
        switchMap(isSignedIn => isSignedIn ? googleAuthService.getUser() : of(null)),
      )
      .subscribe(user => user && signInSuccessAction(user, googleAuthService.authType))

    fbAuthService.isLoggedIn().subscribe(res => console.log(res));

    return () => subscription.unsubscribe();
  }, [container, signInSuccessAction, signOutAction])

  return (
    <div className={'right-nav'}>
      <AnimatePresence exitBeforeEnter>
        {
          user?.data &&
          <Button onClick={() => setIsDrawerOpen(!isDrawerOpen)}
            classes={{ root: 'profile-btn', label: 'profile-text' }}
            variant={'contained'} color={'info'} size={'small'}
            startIcon={ <Avatar className={'profile-thumbnail'} alt="avatar-profile" src={user.data.image}/>}
            component={motion.button} key={'logout-btn'}
            variants={logInButton} initial={'init'} animate={'show'} exit={'init'}>
            {user.data.name}
          </Button>
        }
        {
          !user?.data &&
          <Button onClick={() => setIsDrawerOpen(!isDrawerOpen)}
                  color={'inherit'}
                  component={motion.button} key={'login-btn'}
                  variants={logInButton} initial={'init'} animate={'show'} exit={'init'}>
            Login
          </Button>
        }
      </AnimatePresence>
      <Drawer
        classes={{ paper: 'drawer-container' }}
        ModalProps={{ BackdropProps: { classes: { root: 'modal-backdrop'} } }}
        anchor={'right'} open={isDrawerOpen} onClose={() => setIsDrawerOpen(!isDrawerOpen)}>
        <AnimatePresence exitBeforeEnter>
          { !user?.data && <AuthPanel key={'auth-panel'} setIsDrawerOpen={setIsDrawerOpen}/> }
          { user?.data && <UserPanel key={'user-panel'} setIsDrawerOpen={setIsDrawerOpen} user={user?.data as User} />}
        </AnimatePresence>
      </Drawer>
    </div>
  );
}

const mapStateToProps = (state: AppState) => ({
  user: state.auth
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  signInSuccessAction: (user: User, authType: AuthType) => dispatch(signInSuccessAction(user, authType)),
  signOutAction: () => dispatch(signOutAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);