import { Variants } from 'framer-motion';

export const panelVariants: Variants = {
  show: {
    transition: { staggerChildren: 0.07, delayChildren: 0.2 }
  },
  hide: {
    transition: { staggerChildren: 0.05, staggerDirection: -1 }
  }
};

export const panelItemVariants: Variants = {
  show: {
    y: 0,
    opacity: 1,
    transition: {
      y: { stiffness: 1000, velocity: -100 },
      // duration: .5
    }
  },
  hide: {
    y: 50,
    opacity: 0,
    transition: {
      y: { stiffness: 1000 },
    }
  }
};
export const container: Variants = {
  init: {},
  show: {
    transition: {staggerChildren: 0.07, delayChildren: 0.2}
  }
}
export const logInButton: Variants = {
  init: {
    opacity: 0,
    rotateX: -90
  },
  show: {
    opacity: 1,
    rotateX: 0,
    transition: {
      ease: [0.34, 1.56, 0.64, 1],
      duration: .8
    }
  }
};