import { Dispatch, SetStateAction } from 'react';
import { User, AuthService } from '../../../models';

export interface Props {
  user: User
  setIsDrawerOpen: Dispatch<SetStateAction<boolean>>,
}

export interface ReduxProps {
  signOutWithServiceAction: (authService: AuthService) => void
}

export interface UserPanelProps extends Props, ReduxProps {}