import React, { FunctionComponent, useContext } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { signOutWithServiceAction } from '../../../redux/auth/auth.action';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';

import TYPES from '../../../di-container/di.types';
import { DiContext } from '../../../context/di.context';
import { User, Action, AuthService, AuthType, AuthServiceFactory } from '../../../models';
import { UserPanelProps } from './props';

import './UserPanel.scss';
import podcastSVG from '../../../assets/podcast.svg';
import { motion } from 'framer-motion';
import { panelItemVariants, panelVariants } from '../variants.motion';

const UserPanel: FunctionComponent<UserPanelProps> = ({ setIsDrawerOpen, user, signOutWithServiceAction }) => {
  const container = useContext(DiContext);
  const authService: AuthService = container.get<AuthServiceFactory>(TYPES.ServiceFactory)(user?.authType as AuthType);

  return (
    <Card className={'user-panel'} raised={false} component={motion.div}
          variants={panelVariants} initial={'hide'} animate={'show'} exit={'hide'} >
      <CardHeader
        classes={{ title: 'card-title', subheader: 'card-subheader', action: 'card-action'}}
        action={
          <IconButton aria-label="settings" color={'inherit'} size={'small'} onClick={() => setIsDrawerOpen(false)}>
            <ClearIcon />
          </IconButton>
        }
        avatar={<Avatar className={'profile-pic'} alt="avatar-profile" src={user.image} variant={'rounded'}/>}
        title={user.name} subheader={user.email}
        component={motion.div} variants={panelItemVariants}
      />
      <CardContent classes={{ root: 'quote' }} component={motion.div} variants={panelItemVariants}>
        <FormatQuoteIcon classes={{ root: 'left-quote' }}/>
        <Typography classes={{ root: 'quote-text' }}  variant={'subtitle2'} paragraph align={'center'}>
          I am a gamer not because I don't have a life but I choose to have many.
        </Typography>
        <FormatQuoteIcon classes={{ root: 'right-quote' }}/>
      </CardContent>
      <CardMedia image={podcastSVG} classes={{ root: 'card-media' }} height={350} width={'100%'}
                 component={motion.svg} variants={panelItemVariants}/>
      <CardContent component={motion.div} variants={panelItemVariants}>
        <Button onClick={() => signOutWithServiceAction(authService)}
                color={'error'} variant={'contained'} fullWidth>
          LogOut
        </Button>
      </CardContent>
    </Card>
  )
}

const mapDispatchToProps = (dispatch: ThunkDispatch<void, any, Action<User | string>>) => ({
  signOutWithServiceAction: (authService: AuthService) => dispatch(signOutWithServiceAction(authService))
});

export default connect(null, mapDispatchToProps)(UserPanel);