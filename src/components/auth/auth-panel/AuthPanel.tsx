import React, { Dispatch, FunctionComponent, SetStateAction } from 'react';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

import GoogleOAuth from '../google-oauth/GoogleOAuth';
import FaceBookOAuth from '../facebook-oauth/FaceBookOAuth';
import loginSVG from '../../../assets/login.svg';

import './AuthPanel.scss';
import { motion } from 'framer-motion';
import { panelVariants, panelItemVariants } from '../variants.motion';

const AuthPanel: FunctionComponent<{ setIsDrawerOpen: Dispatch<SetStateAction<boolean>> }> = ({ setIsDrawerOpen}) => {
  return (
    <Card className={'auth-panel'} raised={false} component={motion.div}
          variants={panelVariants} initial={'hide'} animate={'show'} exit={'hide'} >
      <CardHeader
        action={
          <IconButton aria-label="settings" color={'inherit'} size={'small'} onClick={() => setIsDrawerOpen(false)}>
            <ClearIcon />
          </IconButton>
        }
        classes={{ title: 'card-title', subheader: 'card-subheader', action: 'card-action'}}
        title="Welcome to StreamTv"
        subheader="A super streaming platform"
        component={motion.div}
        variants={panelItemVariants}
      />
      <CardMedia image={loginSVG} height={350} width={'100%'} component={motion.svg}
                 variants={panelItemVariants}/>
      <CardContent component={motion.div} variants={panelItemVariants}>
        <GoogleOAuth/>
        <FaceBookOAuth/>
      </CardContent>
    </Card>
  )
}

export default AuthPanel;