import { User, State, AuthType } from '../../models';

interface Props {}

interface ReduxProps {
  user?: State<User>,
  signInSuccessAction: (user: User, authType: AuthType) => void,
  signOutAction: () => void
}

export interface AuthProps extends Props, ReduxProps {}
