import React, { FunctionComponent, useContext } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { signInWithServiceAction } from '../../../redux/auth/auth.action';

import Google from '@material-ui/icons/Google';
import Button from '@material-ui/core/Button';

import TYPES from '../../../di-container/di.types';
import { DiContext } from '../../../context/di.context';
import { User, AuthService, Action, AuthServiceFactory, AuthType } from '../../../models';
import './GoogleOAuth.scss';

const GoogleOAuth: FunctionComponent<{ signInWithServiceAction: (service: AuthService) => void }> = ({ signInWithServiceAction }) => {
  const container = useContext(DiContext);
  const googleAuthService: AuthService = container.get<AuthServiceFactory>(TYPES.ServiceFactory)(AuthType.Google);

  return (
    <Button
      variant="contained"
      color="error"
      size="large"
      fullWidth
      className={'google-btn'}
      startIcon={<Google/>}
      onClick={() => signInWithServiceAction(googleAuthService)}
    >
      Login with google
    </Button>
  );
}

const mapDispatchToProps = (dispatch: ThunkDispatch<void, any, Action<User | string>>) => ({
  signInWithServiceAction: (authService: AuthService) => dispatch(signInWithServiceAction(authService))
});

export default connect(null, mapDispatchToProps)(GoogleOAuth);