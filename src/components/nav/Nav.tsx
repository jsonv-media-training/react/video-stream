import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import CameraTwoToneIcon from '@material-ui/icons/CameraTwoTone';

import './Nav.scss';
import Auth from '../auth/Auth';

const Nav: FunctionComponent<any> = () => {

  return (
    <AppBar position="static" >
      <Toolbar variant="dense">
        <Link to={'/'} className={'nav-icon'}>
          <Button color={'inherit'}>
            <CameraTwoToneIcon fontSize={'large'}/>
            <p className={'brand-name'}>Stream Tv.</p>
          </Button>
        </Link>
        <Auth/>
      </Toolbar>
    </AppBar>
  )
};

export default Nav;