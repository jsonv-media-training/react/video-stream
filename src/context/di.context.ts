import { createContext } from 'react';
import { Container } from 'inversify';
import container from '../di-container/di.config';

export const DiContext = createContext<Container>(container);