import { forkJoin, Observable, of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { User, AuthService, AuthType, APIProvider } from '../models';
import { FaceBook } from '../app/extended-type';
import { inject, injectable } from 'inversify';
import TYPES from '../di-container/di.types';

@injectable()
export class FacebookAuthService implements AuthService {
  private static defaultScope = 'email public_profile';
  private static defaultFields: FaceBook.UserField[] = ['id', 'name', 'email'];
  private static STATUS = {
    CONNECTED: 'connected',
    AUTHORIZATION_EXPIRED: 'authorization_expired',
    NOT_AUTHORIZED: 'not_authorized',
    UNKNOWN: 'unknown'
  }
  authType = AuthType.FaceBook;

  @inject(TYPES.FacebookAPIProvider)
  private _fbAuth$?: APIProvider<FaceBook.FacebookStatic>

  constructor() { console.log('FacebookAuthService constructor run! should run only 1'); }

  get fbAuth(): Observable<FaceBook.FacebookStatic | null> {
    return this._fbAuth$!.getAPI$().asObservable()
  }

  private _login(options: FaceBook.LoginOptions): Observable<FaceBook.StatusResponse> {
    return this.fbAuth.pipe(
      switchMap(fbAuth => fbAuth ?
        new Observable<FaceBook.StatusResponse>((observer) => {
          fbAuth.login((res: FaceBook.StatusResponse) => {
            if (!res.authResponse || res.status !== FacebookAuthService.STATUS.CONNECTED) {
              observer.error(new Error(`failed to login: status=${res.status}`));
            }
            console.log('called');
            observer.next(res);
            observer.complete();
          }, options);
        }) :
        throwError(new Error('fbAuth was not initialized'))
      ),
    )
  }

  private _getUserInfo = (fbAuth: FaceBook.FacebookStatic, userId: string, fields: FaceBook.UserField[]) =>
    new Observable<User>((observer) => {
      fbAuth.api(
        `/${userId}/`,
        { fields: fields.join(',') },
        (res: User & FaceBook.Error) => {
          if (res.error) {
            const { error: { message, code } } = res as FaceBook.Error;
            observer.error(new Error(`failed to get fb user info with code=${code}: ${message}`));
          }
          observer.next(res as User);
          observer.complete();
        });
    });

  private _getUserPicture = (fbAuth: FaceBook.FacebookStatic, userId: string) =>
    new Observable<{ url: string }>((observer) => {
      fbAuth.api(
        `/${userId}/picture`,
        { fields: 'url', redirect: false, type: 'normal', width: 200, height: 200 },
        (res: FaceBook.ProfilePicture & FaceBook.Error) => {
          if (res.error) {
            const { error: { message, code } } = res as FaceBook.Error;
            observer.error(new Error(`failed to get fb user profile picture with code=${code}: ${message}`));
          }
          observer.next(res.data);
          observer.complete();
        });
    });

  isLoggedIn(): Observable<boolean> {
    return this.fbAuth.pipe(
      switchMap(fbAuth => fbAuth ? new Observable<boolean>((observer) => {
        fbAuth.getLoginStatus((res: FaceBook.StatusResponse) => {
          observer.next(res.status === FacebookAuthService.STATUS.CONNECTED);
          observer.complete();
        });
      }) : of(false))
    );
  }

  getUser(userId: string, fields: FaceBook.UserField[]): Observable<User> {
    return this.fbAuth.pipe(
      switchMap(fbAuth => fbAuth ?
        forkJoin([this._getUserInfo(fbAuth, userId, fields), this._getUserPicture(fbAuth, userId)]) :
        throwError(new Error('fbAuth was not initialized'))
      ),
      map(res => {
        const user: User = { ...res[0], image: res[1].url }
        return user;
      })
    )
  }

  login(
    options: FaceBook.LoginOptions = { scope: FacebookAuthService.defaultScope },
    fields: FaceBook.UserField[] = FacebookAuthService.defaultFields
  ): Observable<any> {
    return this._login(options)
      .pipe(switchMap(res => this.getUser(res.authResponse.userID, fields)))
  }

  logout(): Observable<void> {
    return this.fbAuth.pipe(
      switchMap(fbAuth => fbAuth ?
        new Observable<void>((observer) => {
          fbAuth?.logout((res: FaceBook.StatusResponse) => {
            observer.next();
            observer.complete();
          })
        }) :
        throwError(new Error('fbAuth was not initialized'))
      )
    );
  }
}