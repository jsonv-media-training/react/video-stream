import { BehaviorSubject, iif, Observable, of, throwError } from 'rxjs';
import { concatMap, delay, retryWhen, take, tap } from 'rxjs/operators';
import { injectable } from 'inversify';
import { Google } from '../app/extended-type';
import { APIProvider } from '../models';

@injectable()
export class GoogleAPIProvider implements APIProvider<Google.GoogleAuth> {
  private googleAuth$ = new Observable<Google.GoogleAuth>((observer) => {
    const { gapi } = window;
    if (!gapi) observer.error(new Error('gapi was not loaded'));
    gapi.load(
      'client:auth2',
      {
        callback: () => {
          gapi.client.init({
            clientId: '441271380111-dsjkdbegl74h1hnrfs3k69ql8i4m094l.apps.googleusercontent.com',
            scope: 'profile email'
          })
            .then(() => {
              observer.next(gapi.auth2.getAuthInstance());
              observer.complete();
            })
            .catch((err: Google.GoogleError) => observer.error(new Error(`failed to initialize gapi client: ${err.error}, ${err.detail}`)))
        },
        onerror: () => observer.error(new Error(`an unknown error occurred while loading 'client:auth2'`))
      }
    );
  })
  .pipe(
    retryWhen(errs => errs.pipe(
      concatMap((err, i) =>
        iif(
          () => i > 5,
          throwError(new Error('retry loading gapi reach limit!')),
          of(err).pipe(delay(100))
        )
      )
    )),
    tap(googleAuth => this._googleAuthBehavior$.next(googleAuth))
  );

  private _googleAuthBehavior$: BehaviorSubject<Google.GoogleAuth | null> = new BehaviorSubject<Google.GoogleAuth | null>(null);

  constructor() {
    this.googleAuth$.subscribe(
      () => {},
      error => console.log(error)
    );
  }

  getAPI$(): BehaviorSubject<Google.GoogleAuth | null> {
    return this._googleAuthBehavior$;
  }
}