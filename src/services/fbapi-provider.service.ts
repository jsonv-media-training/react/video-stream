import { BehaviorSubject, iif, Observable, of, throwError } from 'rxjs';
import { concatMap, delay, retryWhen, take, tap } from 'rxjs/operators';
import { injectable } from 'inversify';
import { FaceBook } from '../app/extended-type';
import { APIProvider } from '../models';

@injectable()
export class FaceBookAPIProvider implements APIProvider<FaceBook.FacebookStatic> {
  private fbAuth$ = new Observable<FaceBook.FacebookStatic>((observer) => {
    const { FB } = window;
    if (!FB) observer.error(new Error('FB api was not loaded'));
    FB?.init({
      appId: '286175969625405',
      autoLogAppEvents : true,
      status: true,
      xfbml: true,
      // cookie: true,
      version: 'v10.0'
    });
    observer.next(FB);
    observer.complete();
  })
  .pipe(
    retryWhen(errs => errs.pipe(
      concatMap((err, i) =>
        iif(
          () => i > 5,
          throwError(new Error('retry loading gapi reach limit!')),
          of(err).pipe(delay(100))
        )
      )
    )),
    tap(fbAuth => this._fbAuthBehavior$.next(fbAuth))
  );

  private _fbAuthBehavior$: BehaviorSubject<FaceBook.FacebookStatic | null> = new BehaviorSubject<FaceBook.FacebookStatic | null>(null);

  constructor() {
    this.fbAuth$.pipe().subscribe(
      // fbAuth => this._fbAuthBehavior$.next(fbAuth),
      () => {},
      error => console.log(error)
    );
  }

  getAPI$(): BehaviorSubject<FaceBook.FacebookStatic | null> {
    return this._fbAuthBehavior$;
  }
}