import { inject, injectable } from 'inversify';
import TYPES from '../di-container/di.types';

import { from, Observable, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Google } from '../app/extended-type';
import { AuthService, AuthType, APIProvider, User } from '../models';

@injectable()
export class GoogleAuthService implements AuthService {
  authType = AuthType.Google;

  @inject(TYPES.GoogleAPIProvider)
  private _googleAuth$?: APIProvider<Google.GoogleAuth>

  constructor() { console.log('GoogleAuthService constructor run! should run only 1'); }

  get googleAuth(): Observable<Google.GoogleAuth | null> {
    return this._googleAuth$!.getAPI$().asObservable();
  }

  _googleUserToUser = (googleUser: Google.GoogleUser) => ({
    id: googleUser.getBasicProfile().getId(),
    email: googleUser.getBasicProfile().getEmail(),
    name: googleUser.getBasicProfile().getName(),
    image: googleUser.getBasicProfile().getImageUrl()
  });

  isLoggedIn(): Observable<boolean> {
    return this.googleAuth.pipe(
      map(googleAuth => googleAuth ? googleAuth.isSignedIn.get() : false),
    );
  }

  getUser(): Observable<User> {
    return this.googleAuth.pipe(
      map(googleAuth => {
        if (!googleAuth) throw new Error('googleAuth was not initialized')
        return this._googleUserToUser(googleAuth!.currentUser.get())
      })
    )
  }

  login(): Observable<User> {
    return this.googleAuth.pipe(
      switchMap(googleAuth => googleAuth ?
        from(googleAuth.signIn()).pipe(map(googleUser => this._googleUserToUser(googleUser))) :
        throwError(new Error('googleAuth was not initialized'))),
      catchError((err: Google.GoogleError & Error) =>  throwError(new Error(err.error ? err.error : err.message)))
    );
  }

  logout(): Observable<void> {
    return this.googleAuth.pipe(
      map(googleAuth => googleAuth ?
        googleAuth.signOut() :
        throwError(new Error('googleAuth was not initialized'))
      )
    );
  }
}